﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ
{
    internal class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */

        private static void Main()
        {
            //PrintOutOfStock(); //D 1.
            //InStockMoreThan3(); //D 2.
            //CustomersInWashington(); //D 3.
            //NamesOfProducts(); //D 4.
            //Increase25Percent(); //D 5.
            //UpperCaseProductNames(); //D 6.
            //EvenStockNumbers(); //D 7.
            //8. Create a new sequence of products with ProductName, Category, and rename UnitPrice to Price. - CREATED
            //ReturnPairsWhereBNumLessThanCNum(); //D 9
            ShowCustOrderID_IfTotalOrder_Under500(); //10
            //FirstThreeEleFromA(); //D 11
            //FirstThreeOrdersFromCustInWA(); //D 12
            //FirstThreeEleOfNumA(); //D 13
            //SkipFirstTwoOrders(); //D 14
            //AllEleInNumCUntil6(); //D 15
            //ReturnNumCEleUntilLessThanPosInArray(); //D 16
            //ReturnEleFromNumCFromFirstEleDivisibleByThree(); //D 17
            //OrderProductsAlphabeticallyByName(); //D 18
            //UnitsInStockDescending();//D 19
            //SortProductsByCatByPriceHiToLow();// 20
            //ReverseNumC(); //D 21
            //GroupNumCEleByRemainerWhenDivBy5(); //D 22
            //ProdByCat();//D 23
            //CustomerOrdersByYearThenMonth(); //D 24
            //UniqueProductCategoryNamesList(); //D 25
            //UniqueValuesFromNumAAndNumB(); //D 26
            //SharedNumsFromAAndB();//D 27
            //ValuesInNumANotInNumB();//D 28
            //FirstProdWithProdID12();//D 29
            //SeeIfProdID789Exists();//D 30
            //ListCatIfAtLeast1ProdOutOfStock();//D 31
            //DoesNumbersBContainOnlyNumLessThan9(); //D 32
            //ListCategoriesIfProductInStock(); //D 33
            //CountNumOfOddsInNumbersA(); //D 34
            //CustIDsAndOrderCount();//D 35
            //CategoriesAndProductCount();//D 36
            //TotalUnitsInStockForEachCategory();//D 37
            //GetLowestProductInCategories(); //D 38 
            //GetHighestProductInCategories(); //D 39
            //GetAverageProductInCategories(); //D 40

            Console.ReadLine();
        }

        //D 1. Find all products that are out of stock.
        private static void PrintOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            //var results = products.Where(p => p.UnitsInStock == 0);
            var results = from p in products
                          where p.UnitsInStock == 0
                          select p;

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        //D 2. Find all products that are in stock and cost more than 3.00 per unit.
        private static void InStockMoreThan3()
        {
            var products = DataLoader.LoadProducts();

            //var results = products.Where(p => p.UnitsInStock > 0 && p.UnitPrice > 3);
            var results = from p in products
                          where p.UnitsInStock > 0 && p.UnitPrice > 3
                          select p;

            foreach (var product in results)
            {
                Console.WriteLine("{0} has {1} in stock with unit price {2}", product.ProductName, product.UnitsInStock,
                    product.UnitPrice);
            }
        }

        //D 3. Find all customers in Washington, print their name then their orders. (Region == "WA")
        private static void CustomersInWashington()
        {
            var customers = DataLoader.LoadCustomers();

            var results = customers.Where(c => c.Region == "WA");

            //var resultsProd = customers.ToLookup(p => p.Orders);

            foreach (var customer in results)
            {
                Console.WriteLine("{0} - {1}", customer.CustomerID, customer.Region);
                foreach (var order in customer.Orders)
                {
                    Console.WriteLine("  -" + order.OrderID + ", " + order.OrderDate + ", " + order.Total);
                }
            }


        }

        //D 4. Create a new sequence with just the names of the products.
        private static void NamesOfProducts()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          select p.ProductName;

            foreach (var product in results)
            {
                Console.WriteLine(product);
            }
        }

        //D 5. Create a new sequence of products and unit prices where the unit prices are increased by 25%.
        private static void Increase25Percent()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          select p.UnitPrice;

            //decimal newPrice = results*1.25;

            foreach (var product in results)
            {
                Console.WriteLine(product * (decimal)1.25);
            }
        }

        //D 6. Create a new sequence of just product names in all upper case.
        private static void UpperCaseProductNames()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          select p.ProductName.ToUpper();

            foreach (var product in results)
            {
                Console.WriteLine(product);
            }
        }

        //D 7. Create a new sequence with products with even numbers of units in stock.
        private static void EvenStockNumbers()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          select p.UnitsInStock;

            foreach (var result2 in results)
            {
                if (result2 > 0 && result2 % 2 == 0)
                {
                    Console.WriteLine(result2);
                }
            }
        }

        //8. Create a new sequence of products with ProductName, Category, and rename UnitPrice to Price.


        //D 9. Make a query that returns all pairs of numbers from both arrays such that the number from numbersB is less than the number from numbersC.
        private static void ReturnPairsWhereBNumLessThanCNum()
        {
            var numbersB = DataLoader.NumbersB;
            var numbersC = DataLoader.NumbersC;

            var results = from b in numbersB
                          from c in numbersC
                          where b < c
                          select new { b, c };

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        //D 10. Select CustomerID, OrderID, and Total where the order total is less than 500.00.
        private static void ShowCustOrderID_IfTotalOrder_Under500()
        {
            var customer = DataLoader.LoadCustomers();

            var results = from c in customer
                          from o in c.Orders
                          where o.Total < 500
                          select new { c.CustomerID, o.OrderID, o.Total };

            foreach (var orders in results)
            {
                Console.WriteLine("{0} - {1} {2}", orders.CustomerID, orders.OrderID, orders.Total);
            }

            Console.WriteLine(results);
        }

        //D 11. Write a query to take only the first 3 elements from NumbersA.
        private static void FirstThreeEleFromA()
        {
            var numbersA = DataLoader.NumbersA;

            var results = from n in numbersA
                          select numbersA;

            for (int i = 0; i < numbersA.Length - 1; i++)
            {
                if (i < 3)
                {
                    Console.WriteLine(numbersA[i]);
                }
            }
        }

        //D 12. Get only the first 3 orders from customers in Washington. .Take(3)
        private static void FirstThreeOrdersFromCustInWA()
        {
            var customers = DataLoader.LoadCustomers();

            var results = (from c in customers
                           from o in c.Orders
                           where c.Region == "WA"
                           select new { c.CustomerID, o.OrderID, o.OrderDate }).Take(3);
            //var results = customers.Where(c => c.Region == "WA").Select(c => c.Orders).Take(3);

            foreach (var customer in results)
            {
                Console.WriteLine(customer);
            }
        }

        //D 13. Skip the first 3 elements of NumbersA.
        private static void FirstThreeEleOfNumA()
        {
            var numbersA = DataLoader.NumbersA;

            var result = numbersA;

            for (int i = 2; i < result.Length; i++)
            {
                Console.WriteLine(result[i]);
            }
        }

        //D 14. Get all except the first two orders from customers in Washington.
        private static void SkipFirstTwoOrders()
        {
            var customers = DataLoader.LoadCustomers();

            var results = (from c in customers
                           from o in c.Orders
                           where c.Region == "WA"
                           select new { c.CustomerID, o.OrderID, o.OrderDate }).Skip(2);

            foreach (var customer in results)
            {
                Console.WriteLine(customer);
            }
        }

        //D 15. Get all the elements in NumbersC from the beginning until an element is greater or equal to 6.
        private static void AllEleInNumCUntil6()
        {
            var numbersC = DataLoader.NumbersC;

            var result = numbersC.TakeWhile(n => n < 6);

            foreach (var i in result)
            {
                Console.WriteLine(i);
            }

        }

        //D 16. Return elements starting from the beginning of NumbersC until a number is hit that is less than its position in the array.
        private static void ReturnNumCEleUntilLessThanPosInArray()
        {
            var numbersC = DataLoader.NumbersC;

            var result = numbersC.TakeWhile((n, index) => n >= index);

            foreach (var i in result)
            {
                Console.WriteLine(i);
            }
        }

        //D 17. Return elements from NumbersC starting from the first element divisible by 3.
        private static void ReturnEleFromNumCFromFirstEleDivisibleByThree()
        {
            var numbersC = DataLoader.NumbersC;

            var result = numbersC.SkipWhile(n => n % 3 != 0);

            foreach (var i in result)
            {
                Console.WriteLine(i);
            }

            //for (int i = 0; i < result.Length; i++)
            //{
            //    if (result[i] % 3 == 0)
            //    {
            //        while (i < result.Length)
            //        {
            //            Console.WriteLine(result[i]);
            //            i++;
            //        }
            //    }
            //}
        }

        //D 18. Order products alphabetically by name.
        private static void OrderProductsAlphabeticallyByName()
        {
            var products = DataLoader.LoadProducts();

            //var results = from p in products
            //    orderby p.ProductName.ToList()
            //    select p;
            var results = products.OrderBy(x => x.ProductName).ToList();

            foreach (var result in results)
            {
                Console.WriteLine(result.ProductName);
            }
        }

        //D 19. Order products by UnitsInStock descending.
        private static void UnitsInStockDescending()
        {
            var products = DataLoader.LoadProducts();

            var result = products.OrderByDescending(p => p.UnitsInStock);

            foreach (var product in result)
            {
                Console.WriteLine(product.UnitsInStock);
            }
        }

        //D 20. Sort the list of products, first by category, and then by unit price, from highest to lowest.
        private static void SortProductsByCatByPriceHiToLow()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          orderby p.Category, p.UnitPrice descending
                          select p;
            //var results = products.OrderBy(prod => prod.Category).ThenByDescending(prod => prod.UnitPrice);

            foreach (var result in results)
            {
                Console.WriteLine("{0}, {1}, {2}", result.ProductName, result.Category, result.UnitPrice);
            }
        }

        //D 21. Reverse NumbersC.
        private static void ReverseNumC()
        {
            var numbersC = DataLoader.NumbersC;

            var result = numbersC;

            foreach (var i in result.Reverse())
            {
                Console.WriteLine(i);
            }
        }

        //D 22. Display the elements of NumbersC grouped by their remainder when divided by 5.
        private static void GroupNumCEleByRemainerWhenDivBy5()
        {
            var numC = DataLoader.NumbersC;

            var results = from n in numC
                          group n by n % 5
                          into nMod
                          select new { Rem = nMod.Key, coll = nMod };

            foreach (var result in results)
            {
                Console.WriteLine("Numbers with a remainder of {0} when divided by 5:", result.Rem);
                foreach (var i in result.coll)
                {
                    Console.WriteLine(i);
                }
            }
        }

        //D 23. Display products by Category.
        private static void ProdByCat()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          group p by p.Category
                into pCats
                          select new
                          {
                              pCats.Key, coll = pCats
                          };

            foreach (var result in results)
            {
                Console.WriteLine(result.Key);
                foreach (var product in result.coll)
                {
                    Console.WriteLine("\t" + product.ProductName);
                }
            }
        }

        //24. Group customer orders by year, then by month.
        private static void CustomerOrdersByYearThenMonth()
        {
            var customers = DataLoader.LoadCustomers();

            var results = from c in customers
                          select new
                          {
                              Company = c.CompanyName,
                              YearGroups = from o in c.Orders
                                           group o by o.OrderDate.Year
                                           into yg
                                           select new
                                           {
                                               Year = yg.Key,
                                               MonthGroups = from o in yg
                                                             group o by o.OrderDate.Month
                                                             into mg
                                                             select new
                                                             {
                                                                 Month = mg.Key,
                                                                 Orders = mg
                                                             }
                                           }
                          };

            foreach (var result in results)
            {
                Console.WriteLine(result.Company);
                foreach (var yearGroup in result.YearGroups)
                {
                    Console.WriteLine(yearGroup.Year);
                    foreach (var monthGroup in yearGroup.MonthGroups)
                    {
                        Console.Write(monthGroup.Month + " ");
                    }
                    Console.WriteLine("\n");
                }
            }

        }

        //D 25. Create a list of unique product category names.
        private static void UniqueProductCategoryNamesList()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          select p.Category;

            var unique = results.GroupBy(x => x).Select(y => y.First());

            foreach (var result in unique)
            {
                Console.WriteLine(result);
            }

        }

        //D 26. Get a list of unique values from NumbersA and NumbersB.
        private static void UniqueValuesFromNumAAndNumB()
        {
            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;

            var results = numbersA.Except(numbersB);
            var results2 = numbersB.Except(numbersA);
            var finres = results.Union(results2);

            foreach (var result in finres)
            {
                Console.WriteLine(result);
            }
            Console.WriteLine();
        }

        //D 27. Get a list of the shared values from NumbersA and NumbersB.
        private static void SharedNumsFromAAndB()
        {
            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;

            var results = numbersA.Intersect(numbersB);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        //D 28. Get a list of values in NumbersA that are not also in NumbersB.
        private static void ValuesInNumANotInNumB()
        {
            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;

            var results = numbersA.Except(numbersB);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }

        }

        //D 29. Select only the first product with ProductID = 12 (not a list).
        private static void FirstProdWithProdID12()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          where p.ProductID == 12
                          select p;

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName + ", " + product.ProductID);
            }
        }

        //D 30. Write code to check if ProductID 789 exists.
        private static void SeeIfProdID789Exists()
        {
            var products = DataLoader.LoadProducts();
            int check = 0;

            var results = from p in products
                              //where p.ProductID == 789 //Solves it, but no way to reply with feedback to user.
                          select p;

            foreach (var product in results)
            {
                if (product.ProductID == 789)
                {
                    Console.WriteLine(product.ProductID);
                }
                else check++;

                if (check == results.Count())
                {
                    Console.WriteLine("ProductID 789 not found.");
                }
            }
        }

        //D 31. Get a list of categories that have at least one product out of stock.
        private static void ListCatIfAtLeast1ProdOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          group p by p.Category
                into pCats
                          where pCats.Any(x => x.UnitsInStock == 0)
                          select new { pCats.Key, coll = pCats };

            foreach (var result in results)
            {
                Console.WriteLine("{0}", result.Key);
                foreach (var product in result.coll)
                {
                    Console.WriteLine("\t{0} - {1}", product.UnitsInStock, product.ProductName);
                }
            }
        }

        //D 32. Determine if NumbersB contains only numbers less than 9.
        private static void DoesNumbersBContainOnlyNumLessThan9()
        {
            var numB = DataLoader.NumbersB;

            var results = from n in numB
                          select n;

            foreach (var result in results)
            {
                if (result >= 9)
                {
                    Console.WriteLine("Numbers B contains numbers 9 or greater.");
                }
                else Console.WriteLine("Numbers B does not contain any numbers greater than 8.");
            }
        }


        //D 33. Get a grouped list of products only for categories that have all of their products in stock.
        private static void ListCategoriesIfProductInStock()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          group p by p.Category
                          into pCats
                          where pCats.All(x => x.UnitsInStock > 0)
                          select new { pCats.Key, coll = pCats };

            foreach (var result in results)
            {
                Console.WriteLine("{0}", result.Key);
                foreach (var product in result.coll)
                {
                    Console.WriteLine("\t{0} - {1}", product.UnitsInStock, product.ProductName);
                }
            }
        }
        //D 34. Count the number of odds in NumbersA.
        private static void CountNumOfOddsInNumbersA()
        {
            var Nums = DataLoader.NumbersA;
            int finalCount = 0;

            var results = from a in Nums
                          where (a % 2 != 0)
                          select a;

            foreach (var result in results)
            {
                Console.WriteLine(++finalCount + " odds.");
            }
        }
        //D 35. Display a list of CustomerIDs and only the count of their orders.
        private static void CustIDsAndOrderCount()
        {
            var customer = DataLoader.LoadCustomers();

            var results = from c in customer
                          from o in c.Orders
                          group c by c.CustomerID
                          into cID
                          select new
                          {
                              cID.Key,
                              orderCount = cID.Count()
                          };

            foreach (var result in results)
            {
                Console.WriteLine("{0} - {1}", result.Key, result.orderCount);
            }
        }

        //D 36. Display a list of categories and the count of their products.
        private static void CategoriesAndProductCount()
        {
            var product = DataLoader.LoadProducts();

            var results = from p in product
                          group p by p.Category
                into pCats
                          select new
                          {
                              pCats.Key,
                              prodCount = pCats.Count()
                          };

            //var results2 = product.Count;

            foreach (var result in results)
            {
                Console.WriteLine("{0} - {1}", result.Key, result.prodCount);
            }
        }

        //D 37. Display the total units in stock for each category.
        private static void TotalUnitsInStockForEachCategory()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          group p by p.Category
                          into pCats
                          select new
                          {
                              pCats.Key,
                              unitsInStock = pCats.Sum(p => p.UnitsInStock)
                          };

            foreach (var result in results)
            {
                Console.WriteLine("{0} - {1}", result.Key, result.unitsInStock);
            }
        }

        //D 38. Display the lowest priced product in each category.
        private static void GetLowestProductInCategories()
        {
            var products = DataLoader.LoadProducts();

            var results2 = from p in products
                           group p by p.Category
                into pCats
                           select new
                           {
                               pCats.Key,
                               minprod = pCats.Min(p => p.UnitPrice),
                               prodName = pCats.First(prod => prod.UnitPrice == pCats.Min(p => p.UnitPrice))
                           };

            var results = products.GroupBy(cat => cat.Category)
             .Select(x => new
             {
                 x.Key,
                 maxProd = x.Min(p => p.UnitPrice),
                 prodName =
             x.First(prod => prod.UnitPrice == x.Min(p => p.UnitPrice))
             });

            foreach (var result in results)
            {
                Console.WriteLine("{0} - {1} {2}", result.Key, result.maxProd, result.prodName.ProductName);
            }
        }

        //D 39. Display the highest priced product in each category.
        private static void GetHighestProductInCategories()
        {
            var products = DataLoader.LoadProducts();

            var results2 = from p in products
                           group p by p.Category
                           into pCats
                           select new
                           {
                               pCats.Key,
                               maxprod = pCats.Max(p => p.UnitPrice),
                               prodName = pCats.First(prod => prod.UnitPrice == pCats.Max(p => p.UnitPrice))
                           };

            var results = products.GroupBy(cat => cat.Category)
             .Select(x => new
             {
                 x.Key,
                 maxProd = x.Max(p => p.UnitPrice),
                 prodName =
             x.First(prod => prod.UnitPrice == x.Max(p => p.UnitPrice))
             });

            foreach (var result in results)
            {
                Console.WriteLine("{0} - {1} {2}", result.Key, result.maxProd, result.prodName.ProductName);
            }
        }

        //D 40. Show the average price of product for each category.
        private static void GetAverageProductInCategories()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          group p by p.Category into pCat
                          select new { Category = pCat.Key, AveragePrice = pCat.Average(p => p.UnitPrice) };

            foreach (var result in results)
            {
                Console.WriteLine("{0} - {1}", result.Category, result.AveragePrice);
            }
        }
    }
}
