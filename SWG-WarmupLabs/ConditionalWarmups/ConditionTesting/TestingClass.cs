﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ConditionalWarmups;

namespace ConditionTesting
{
    public class TestingClass
    {
        private ConditionClass _condTester;

        //1. AreWeInTrouble
        [TestCase(true, true, true)]
        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        public void if_both_orNeither_Trouble_isTrue(bool aSmile, bool bSmile, bool expectedResult)
        {
            bool result = ConditionClass.AreWeInTrouble(aSmile, bSmile);

            Assert.AreEqual(expectedResult, result);
        }
        //2. SleepingIn
        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        [TestCase(false, true, true)]
        public void Vacations_Mean_Sleep_In_Or_Not_Weekday(bool isWeekday, bool isVacation, bool expectedResult)
        {
            bool result = ConditionClass.CanSleepIn(isWeekday, isVacation);

            Assert.AreEqual(expectedResult, result);
        }
        //3. SumDouble
        [TestCase(1, 2, 3)]
        [TestCase(3, 2, 5)]
        [TestCase(2, 2, 8)]
        public void ifTwo_intAre_Same_Double_ElseAdd(int a, int b, int expectedResult)
        {
            int result = ConditionClass.SumDouble(a, b);

            Assert.AreEqual(expectedResult, result);
        }
        //4. Diff21
        [TestCase(23, 4)]
        [TestCase(10, 11)]
        [TestCase(21, 0)]
        public void Diff21(int n, int expectedResult)
        {
            int result = ConditionClass.Diff21(n);

            Assert.AreEqual(expectedResult, result);
        }
        //5. ParrotTrouble
        [TestCase(true, 6, true)]
        [TestCase(true, 7, false)]
        [TestCase(false, 6, false)]
        public void ParrotTrouble_if_Parrot_Talking_When_Shouldnt(bool isTalking, int hour, bool expectedResult)
        {
            bool result = ConditionClass.ParrotTrouble(isTalking, hour);

            Assert.AreEqual(expectedResult, result);
        }
        //6. Makes10
        [TestCase(9, 10, true)]
        [TestCase(9, 9, false)]
        [TestCase(1, 9, true)]
        public void Makes10(int a, int b, bool expectedResult)
        {
            bool result = ConditionClass.Makes10(a, b);

            Assert.AreEqual(expectedResult, result);
        }
        //7. NearHundred
        [TestCase(103, true)]
        [TestCase(90, true)]
        [TestCase(89, false)]
        public void NearHundred(int n, bool expectedResult)
        {
            bool result = ConditionClass.NearHundred(n);

            Assert.AreEqual(expectedResult, result);
        }
        //8. PosNeg
        [TestCase(1, -1, false, true)]
        [TestCase(-1, 1, false, true)]
        [TestCase(-4, -5, true, true)]
        public void PosNeg(int a, int b, bool negative, bool expectedResult)
        {
            bool result = ConditionClass.PosNeg(a, b, negative);

            Assert.AreEqual(expectedResult, result);
        }
        //9. NotString
        [TestCase("candy", "not candy")]
        [TestCase("x", "not x")]
        [TestCase("not bad", "not bad")]
        public void NotString(string s, string expectedResult)
        {
            string result = ConditionClass.NotString(s);

            Assert.AreEqual(expectedResult, result);
        }
        //10. MissingChar
        [TestCase("kitten", 1, "ktten")]
        [TestCase("kitten", 0, "itten")]
        [TestCase("kitten", 4, "kittn")]
        public void MissingString(string str, int n, string expectedResult)
        {
            string result = ConditionClass.MissingChar(str, n);

            Assert.AreEqual(expectedResult, result);
        }
        //11. FrontBack
        [TestCase("code", "eodc")]
        [TestCase("a", "a")]
        [TestCase("ab", "ba")]
        public void FrontBack(string str, string expectedResult)
        {
            string result = ConditionClass.FrontBack(str);

            Assert.AreEqual(expectedResult, result);
        }
        //12. Front3
        [TestCase("Microsoft", "MicMicMic")]
        [TestCase("Chocolate", "ChoChoCho")]
        [TestCase("at", "atatat")]
        public void Front3(string str, string expectedResult)
        {
            string result = ConditionClass.Front3(str);

            Assert.AreEqual(expectedResult, result);
        }
        //13. BackAround
        [TestCase("cat", "tcatt")]
        [TestCase("Hello", "oHelloo")]
        [TestCase("a", "aaa")]
        public void BackAround(string str, string expectedResult)
        {
            string result = ConditionClass.BackAround(str);

            Assert.AreEqual(expectedResult, result);
        }
        //14. Multiple3or5
        [TestCase(3, true)]
        [TestCase(10, true)]
        [TestCase(8, false)]
        public void Multiple3or5(int number, bool expectedResult)
        {
            bool result = ConditionClass.Multiple3or5(number);

            Assert.AreEqual(expectedResult, result);
        }
        //15. StartHi
        [TestCase("hi there", true)]
        [TestCase("hi", true)]
        [TestCase("high up", true)] //Written to return as false? Confused.
        public void StartHi(string str, bool expectedResult)
        {
            bool result = ConditionClass.StartHi(str);

            Assert.AreEqual(expectedResult, result);
        }
        //16. IcyHot
        [TestCase(120, -1, true)]
        [TestCase(-1, 120, true)]
        [TestCase(2, 120, false)]
        public void IcyHot(int temp1, int temp2, bool expectedResult)
        {
            bool result = ConditionClass.IcyHot(temp1, temp2);

            Assert.AreEqual(expectedResult, result);
        }
        //17. Between10and20
        [TestCase(12, 99, true)]
        [TestCase(21, 12, true)]
        [TestCase(8, 99, false)]
        public void Between10and20(int a, int b, bool expectedResult)
        {
            bool result = ConditionClass.Between10and20(a, b);

            Assert.AreEqual(expectedResult, result);
        }
        //18. HasTeen
        [TestCase(13, 20, 10, true)]
        [TestCase(20, 19, 10, true)]
        [TestCase(20, 10, 12, false)]
        public void HasTeen(int a, int b, int c, bool expectedResult)
        {
            bool result = ConditionClass.HasTeen(a, b, c);

            Assert.AreEqual(expectedResult, result);
        }
        //19. SoAlone
        [TestCase(13, 99, true)]
        [TestCase(21, 19, true)]
        [TestCase(13, 13, false)]
        public void SoAlone(int a, int b, bool expectedResult)
        {
            bool result = ConditionClass.SoAlone(a, b);

            Assert.AreEqual(expectedResult, result);
        }
        //20. RemoveDel
        [TestCase("adelbc", "abc")]
        [TestCase("adelHello", "aHello")]
        [TestCase("adedbc", "adedbc")]
        public void RemoveDel(string str, string expectedResult)
        {
            string result = ConditionClass.RemoveDel(str);

            Assert.AreEqual(expectedResult, result);
        }
        //21. IxStart
        [TestCase("mix snacks", true)]
        [TestCase("pix snacks", true)]
        [TestCase("piz snacks", false)]
        public void IxStart(string str, bool expectedResult)
        {
            bool result = ConditionClass.IxStart(str);

            Assert.AreEqual(expectedResult, result);
        }
        //22. StartOz
        [TestCase("ozymandias", "oz")]
        [TestCase("bzoo", "z")]
        [TestCase("oxx", "o")]
        public void StartOz(string str, string expectedResult)
        {
            string result = ConditionClass.StartOz(str);

            Assert.AreEqual(expectedResult, result);
        }
        //23. Max
        [TestCase(1, 2, 3, 3)]
        [TestCase(1, 3, 2, 3)]
        [TestCase(3, 2, 1, 3)]
        public void Max(int a, int b, int c, int expectedResult)
        {
            int result = ConditionClass.Max(a, b, c);

            Assert.AreEqual(expectedResult, result);
        }
        //24. Closer
        [TestCase(8, 13, 8)]
        [TestCase(13, 8, 8)]
        [TestCase(13, 7, 0)]
        public void Closer(int a, int b, int expectedResult)
        {
            int result = ConditionClass.Closer(a, b);

            Assert.AreEqual(expectedResult, result);
        }
        //25. GotE
        [TestCase("Hello", true)]
        [TestCase("Heelle", true)]
        [TestCase("Heelele", false)]
        public void GotE(string str, bool expectedResult)
        {
            bool result = ConditionClass.GotE(str);

            Assert.AreEqual(expectedResult, result);
        }
        //26. EndUp
        [TestCase("Hello", "HeLLO")]
        [TestCase("hi there", "hi thERE")]
        [TestCase("hi", "HI")]
        public void EndUp(string str, string expectedResult)
        {
            string result = ConditionClass.EndUp(str);

            Assert.AreEqual(expectedResult, result);
        }
        //27. EveryNth
        [TestCase("Miracle", 2, "Mrce")]
        [TestCase("abcdefg", 2, "aceg")]
        [TestCase("abcdefg", 3, "adg")]
        public void EveryNth(string str, int n, string expectedResult)
        {
            string result = ConditionClass.EveryNth(str, n);

            Assert.AreEqual(expectedResult, result);
        }
    }
}
