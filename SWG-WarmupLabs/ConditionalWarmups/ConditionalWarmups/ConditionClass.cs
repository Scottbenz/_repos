﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConditionalWarmups
{
    public class ConditionClass
    {
        //1. AreWeInTrouble
        public static bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            if (aSmile == true && bSmile == true)
            {
                return true;
            }
            if (aSmile == false && bSmile == false)
            {
                return true;
            }
            else return false;
        }
        //2. SleepingIn
        public static bool CanSleepIn(bool isWeekday, bool isVacation)
        {
            if (isWeekday && isVacation || !isWeekday && isVacation || !isWeekday && !isVacation)
            {
                return true;
            }
            if (isWeekday && !isVacation)
            {
                return false;
            }
            else return false;
        }
        //3. SumDouble
        public static int SumDouble(int a, int b)
        {
            if (a != b)
            {
                return a + b;
            }
            else if (a == b)
            {
                return 2 * (a + b);
            }
            else return 0;
        }
        //4. Diff21
        public static int Diff21(int n)
        {
            if (n <= 21)
            {
                return (Math.Abs(n - 21));
            }
            else return 2 * (n - 21);
        }
        //5. ParrotTrouble
        public static bool ParrotTrouble(bool isTalking, int hour)
        {
            if (isTalking && (hour > 20 || hour < 7))
            {
                return true;
            }
            else return false;
        }
        //6. Makes10
        public static bool Makes10(int a, int b)
        {
            if (a == 10 || b == 10)
            {
                return true;
            }
            else if (a + b == 10)
            {
                return true;
            }
            else return false;
        }
        //7. NearHundred
        public static bool NearHundred(int n)
        {
            return ((Math.Abs(100 - n) <= 10) || (Math.Abs(200 - n) <= 10));
        }
        //8. PosNeg
        public static bool PosNeg(int a, int b, bool negative)
        {
            if ((a > 0 || b > 0) && !negative)
            {
                return true;
            }
            else if (a < 0 && b < 0)
            {
                return true;
            }
            else return false;
        }
        //9. NotString
        public static string NotString(string s)
        {
            string newstring = "";
            if (s.Contains("not"))
            {
                return s;
            }
            else
            {
                return newstring += "not " + s;
            }
        }
        //10. MissingChar
        public static string MissingChar(string str, int n)
        {
            string newstring = "";

            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str.Substring(0) == str.Substring(n))
                {
                    return str.Remove(n, 1);
                }
                else if (str.Substring(i) == str.Substring(n))
                {
                    return str.Remove(n, 1);
                }
            }
            return newstring += str.Remove(n);
        }
        //11. FrontBack
        public static string FrontBack(string str)
        {
            if (str.Length == 1)
            {
                return str;
            }
            string FirstChar = str.Substring(0, 1);
            string SecondChar = str.Substring(str.Length - 1);
            string newString = "";

            newString += str.Substring(1, str.Length - 2);

            return SecondChar += newString + FirstChar;
        }
        //12. Front3
        public static string Front3(string str)
        {
            if (str.Length < 3)
            {
                return str + str + str;
            }
            return str.Substring(0, 3) + str.Substring(0, 3) + str.Substring(0, 3);
        }
        //13. BackAround
        public static string BackAround(string str)
        {
            string LastChar = str.Substring(str.Length - 1);

            return LastChar += str + LastChar;
        }
        //14. Multiple3or5
        public static bool Multiple3or5(int number)
        {
            if (number % 3 == 0 || number % 5 == 0)
            {
                return true;
            }
            else return false;
        }
        //15. StartHi
        public static bool StartHi(string str)
        {
            string HiCompare = "hi";
            if (str.Substring(0, 2) == HiCompare)
            {
                return true;
            }
            return false;
        }
        //16. IcyHot
        public static bool IcyHot(int temp1, int temp2)
        {
            if (temp1 < 0 && temp2 > 100 || temp1 > 100 && temp2 < 0)
            {
                return true;
            }
            return false;
        }
        //17. Between10and20
        public static bool Between10and20(int a, int b)
        {
            if ((a >= 10 && a <= 20) || b >= 10 && b <= 20)
            {
                return true;
            }
            else return false;
        }
        //18. HasTeen
        public static bool HasTeen(int a, int b, int c)
        {
            if (a <= 19 && a >= 13 || b <= 19 && b >= 13 || c <= 19 && c >= 13)
            {
                return true;
            }
            return false;
        }
        //19. SoAlone
        public static bool SoAlone(int a, int b)
        {
            if (a <= 19 && a >= 13 && (b > 19 || b < 13))
            {
                return true;
            }
            else if (b <= 19 && b >= 13 && (a > 19 || a < 13))
            {
                return true;
            }
            return false;
        }
        //20. RemoveDel
        public static string RemoveDel(string str)
        {
            string newString = "";
            for (int i = 1; i < str.Length - 2; i++)
            {
                if (str.Substring(i, i + 2) == "del")
                {
                    return newString += str.Remove(i, i + 2);
                }
                return str;
            }
            return str;
        }
        //21. IxStart
        public static bool IxStart(string str)
        {
            if (str.Substring(1, 2) == "ix")
            {
                return true;
            }
            return false;
        }
        //22. StartOz
        public static string StartOz(string str)
        {
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str.StartsWith("o"))
                {
                    if (str.Substring(0, 2) == "oz")
                    {
                        return str.Substring(0, 2);
                    }
                    return str.Substring(0, 1);
                }
                if (str.Substring(1, 1) == "z")
                {
                    return str.Substring(1, 1);
                }
            }
            return str;
        }
        //23. Max
        public static int Max(int a, int b, int c)
        {
            if (a > b && a > c)
            {
                return a;
            }
            else if (b > a && b > c)
            {
                return b;
            }
            else return c;
        }
        //24. Closer
        public static int Closer(int a, int b)
        {
            if (Math.Abs(10 - a) > Math.Abs(10 - b))
            {
                return b;
            }
            else if (Math.Abs(10 - a) < Math.Abs(10 - b))
            {
                return a;
            }
            else return 0;
        }
        //25. GotE
        public static bool GotE(string str)
        {
            int count = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str.Substring(i, 1) == "e")
                {
                    count++;
                    if (count > 3)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        //26. EndUp
        public static string EndUp(string str)
        {
            if (str.Length > 2)
            {
                return str.Substring(0, str.Length -3) + str.Substring(str.Length - 3, 3).ToUpper();
            }
            else return str.ToUpper();
        }
        //27. EveryNth
        public static string EveryNth(string str, int n)
        {
            string newString = "";
            for (int i = 0; i < str.Length; i+=n)
            {
                newString += str.Substring(i, 1);
            }
            return newString;
        }
    }
}
