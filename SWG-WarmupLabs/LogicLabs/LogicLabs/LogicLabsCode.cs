﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLabs
{
    public class LogicLabsCode
    {
        //1. GreatParty
        public static bool GreatParty(int cigars, bool isWeekend) 
        {
            if (cigars >= 40 && cigars <= 60)
            {
                return true;
            }
            else if (isWeekend && cigars >= 40)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //2. CanHazTable
        public static int CanHazTable(int yourStyle, int dateStyle) 
        {
            if (yourStyle >= 8 || dateStyle >= 8)
            {
                return 2;
            }
            else if (yourStyle <= 2 || dateStyle <= 2)
            {
                return 0;
            }
            else 
            {
                return 1;
            }
        }
        //3. PlayOutside
        public static bool PlayOutside(int temp, bool isSummer)
        {
            if (temp >= 60 && temp <= 90 && isSummer == false)
            {
                return true;
            }
            else if (temp >= 60 && temp <=100 && isSummer == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //4. CaughtSpeeding
        public static int CaughtSpeeding(int speed, bool isBirthday)
        {
            if (speed > 65 && speed <= 85 && isBirthday == true)
            {
                return 1;
            }
            if (speed >= 86 && isBirthday == true)
            {
                return 2;
            }
            if (speed <= 65 && isBirthday == true)
            {
                return 0;
            }
            if (speed > 60 && speed <= 80)
            {
                return 1;
            }
            if (speed >= 81)
            {
                return 2;
            }
            else return 0;
        }
        //5. SkipSum
        public static int SkipSum(int a, int b)
        {
            if (a + b < 10)
            {
                return a+b;
            }
            else if ((a + b >= 10) && (a + b <= 19))
            {
                return 20;
            }
            else
            {
                return a + b;
            }
        }
        //6. AlarmClock
        public static string AlarmClock(int day, bool vacation)
        {
            if (day <= 5 && day > 0)
            {
                return "7:00";
            }
            else
            {
                return "10:00";
            }

        }
        //7. LoveSix
        public static bool LoveSix(int a, int b)
        {
            if (a == 6 || b == 6)
            {
                return true;
            }
            else if (a + b == 6)
            {
                return true;
            }
            else return false;
        }
        //8. InRange
        public static bool InRange(int n, bool outsideMode)
        {
            if ((n <= 10 && n >= 1) && outsideMode == false)
            {
                return true;
            }
            else if (outsideMode == true && (n <= 1 || n >= 10))
            {
                return true;
            }
            else return false;
        }
        //9. SpecialEleven
        public static bool SpecialEleven(int n)
        {
            if (n%11 == 0 || n%11 == 1)
            {
                return true;
            }
            else return false;
        }
        //10. Mod20
        public static bool Mod20(int n)
        {
            if (n%20 == 0)
            {
                return false;
            }
            else if (n%20 == 1 || n%20 == 2)
            {
                return true;
            }
            else return false;
        }
        //11. Mod35
        public static bool Mod35(int n)
        {
            if (n%3 == 0 && n%5 != 0)
            {
                return true;
            }
            else if (n%5 == 0 && n%3 != 0)
            {
                return true;
            }
            else return false;
        }
        //12. AnswerCell
        public static bool AnswerCell(bool isMorning, bool isMom, bool isAsleep)
        {
            if (isAsleep == true)
            {
                return false;
            }
            else if (isMorning == true && isMom == true && isAsleep == false)
            {
                return true;
            }
            else if (isMorning == true && isMom == false && isAsleep == false)
            {
                return false;
            }
            else return true;
        }
        //13. TwoIsOne
        public static bool TwoIsOne(int a, int b, int c)
        {
            if (a + b == c || a + c == b || b + c == a)
            {
                return true;
            }
            else return false;
        }
        //14. AreInOrder
        public static bool AreInOrder(int a, int b, int c, bool bOk)
        {
            if (a < b && b < c && bOk == false)
            {
                return true;
            }
            else if (a > b || b > c && bOk == false)
            {
                return false;
            }
            else if ((b > a || b < a || a == b) && b < c && bOk == true)
            {
                return true;
            }
            else return false;
        }
        //15. InOrderEqual
        public static bool InOrderEqual(int a, int b, int c, bool equalOk)
        {
            if (a < b && b < c && equalOk == false)
            {
                return true;
            }
            else if (equalOk == true && a <= b && b <= c)
            {
                return true;
            }
            else return false;
        }
        //16. LastDigit
        public static bool LastDigit(int a, int b, int c)
        {
            if (a%10 == b%10 || a%10 == c%10 || b%10 == c%10)
            {
                return true;
            }
            else return false;
        }
        //17. RollDice
        public static int RollDice(int die1, int die2, bool noDoubles)
        {
            if (noDoubles == true)
            {
                if (die1 == die2)
                {
                    if (die1 != 6)
                    {
                        return die1 + 1 + die2;
                    }
                    else
                    {
                        return 1 + die2;
                    }
                }
            }
            return die1 + die2;
        }

    }
}
