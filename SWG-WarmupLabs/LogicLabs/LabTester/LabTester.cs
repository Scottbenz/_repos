﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using LogicLabs;
using NUnit.Framework.Constraints;

namespace LabTester
{
    [TestFixture] //Tells Nunit to test the class.
    public class LabTester
    {
        private LogicLabsCode _logicTester;


        //1
        [TestCase(30, false, false)]
        [TestCase(50, false, true)]
        [TestCase(70, true, true)]
        public void Test_cigars_AndWeekend_MakeFor_GreatParty(int cigars, bool isWeekend, bool expectedResult)
        {
            var result = LogicLabsCode.GreatParty(cigars, isWeekend);

            Assert.AreEqual(expectedResult, result);
        }

        //2
        [TestCase(5, 10, 2)]
        [TestCase(5, 2, 0)]
        [TestCase(5, 5, 1)]
        public void Good_Enough_To_CanHazTable(int yourStyle, int dateStyle, int expectedResult)
        {
            int result = LogicLabsCode.CanHazTable(yourStyle, dateStyle);

            Assert.AreEqual(expectedResult, result);
        }

        //3
        [TestCase(70, false, true)]
        [TestCase(95, false, false)]
        [TestCase(95, true, true)]
        public void Too_Hot_To_PlayOutside(int temp, bool isSummer, bool expectedResult)
        {
            bool result = LogicLabsCode.PlayOutside(temp, isSummer);

            Assert.AreEqual(expectedResult, result);
        }

        //4
        [TestCase(60, false, 0)]
        [TestCase(65, false, 1)]
        [TestCase(65, true, 0)]
        public void Caught_Speeding_Maybe_on_BDay(int speed, bool isBirthday, int expectedResult)
        {
            int result = LogicLabsCode.CaughtSpeeding(speed, isBirthday);

            Assert.AreEqual(expectedResult, result);
        }

        //5
        [TestCase(3, 4, 7)]
        [TestCase(9, 4, 20)]
        [TestCase(10, 11, 21)]
        public void TenTo_Nineteen_EqualTwenty(int a, int b, int expectedResult)
        {
            int result = LogicLabsCode.SkipSum(a, b);

            Assert.AreEqual(expectedResult, result);
        }

        //6
        [TestCase(1, false, "7:00")]
        [TestCase(5, false, "7:00")]
        [TestCase(0, false, "10:00")]
        public void AlarmClock_VacationTime_OrNot(int day, bool vacation, string expectedResult)
        {
            string result = LogicLabsCode.AlarmClock(day, vacation);

            Assert.AreEqual(expectedResult, result);
        }

        //7
        [TestCase(6, 4, true)]
        [TestCase(4, 5, false)]
        [TestCase(1, 5, true)]
        public void LoveSix(int a, int b, bool expectedResult)
        {
            bool result = LogicLabsCode.LoveSix(a, b);

            Assert.AreEqual(expectedResult, result);
        }

        //8
        [TestCase(5, false, true)]
        [TestCase(11, false, false)]
        [TestCase(11, true, true)]
        public void InRange_of_1to10_Unless_Reverse_for_outsideMode(int n, bool outsideMode, bool expectedResult)
        {
            bool result = LogicLabsCode.InRange(n, outsideMode);

            Assert.AreEqual(expectedResult, result);
        }

        //9
        [TestCase(22, true)]
        [TestCase(23, true)]
        [TestCase(24, false)]
        public void if_Mod11Zero_or_Mod11and1is1_Good_Else_Bad(int n, bool expectedResult)
        {
            bool result = LogicLabsCode.SpecialEleven(n);

            Assert.AreEqual(expectedResult, result);
        }

        //10
        [TestCase(20, false)]
        [TestCase(21, true)]
        [TestCase(22, true)]
        public void if_Mod20false_if21or22true(int n, bool expectedResult)
        {
            bool result = LogicLabsCode.Mod20(n);

            Assert.AreEqual(expectedResult, result);
        }

        //11
        [TestCase(3, true)]
        [TestCase(10, true)]
        [TestCase(15, false)]
        public void Mod35_if_n_modTo0_with_3or5_butNot_Both_True(int n, bool expectedResult)
        {
            bool result = LogicLabsCode.Mod35(n);

            Assert.AreEqual(expectedResult, result);
        }

        //12
        [TestCase(false, false, false, true)]
        [TestCase(false, false, true, false)]
        [TestCase(true, false, false, false)]
        public void Answer_Phone_ifAwakeinMorning_andIsMom(bool isMorning, bool isMom, bool isAsleep,
            bool expectedResult)
        {
            bool result = LogicLabsCode.AnswerCell(isMorning, isMom, isAsleep);

            Assert.AreEqual(expectedResult, result);
        }

        //13
        [TestCase(1, 2, 3, true)]
        [TestCase(3, 1, 2, true)]
        [TestCase(3, 2, 2, false)]
        public void if_Two_Nums_Equal_Third_True(int a, int b, int c, bool expectedResult)
        {
            bool result = LogicLabsCode.TwoIsOne(a, b, c);

            Assert.AreEqual(expectedResult, result);
        }
        //14
        [TestCase(1, 2, 4, false, true)]
        [TestCase(1, 2, 1, false, false)]
        [TestCase(1, 1, 2, true, true)]
        public void abc_Must_beIn_Order_Unless_bOk_Causes_Havoc(int a, int b, int c, bool bOk, bool expectedResult)
        {
            bool result = LogicLabsCode.AreInOrder(a, b, c, bOk);

            Assert.AreEqual(expectedResult, result);
        }
        //15
        [TestCase(2, 5, 11, false, true)]
        [TestCase(5, 7, 6, false, false)]
        [TestCase(5, 5, 7, true, true)]
        public void InOrderEqual_Unless_equalOk(int a, int b, int c, bool equalOk, bool expectedReturn)
        {
            bool result = LogicLabsCode.InOrderEqual(a, b, c, equalOk);

            Assert.AreEqual(expectedReturn, result);
        }
        //16
       /* [TestCase(23, 19, 13, true)]
        [TestCase(23, 19, 12, false)]
        [TestCase(23, 19, 3, true)]
        public void LastDigit_ofTwo_orMore_Makes_True(int a, int b, int c, bool expectedResult)
        {
            bool result = LogicLabsCode.LastDigit(a, b, c);

            Assert.AreEqual(expectedResult, result);
        }*/
        //17
        [TestCase(2, 3, true, 5)]
        [TestCase(3, 3, true, 7)]
        [TestCase(3, 3, false, 6)]
        public void RollDice(int die1, int die2, bool noDoubles, int expectedReturn)
        {
            int result = LogicLabsCode.RollDice(die1, die2, noDoubles);

            Assert.AreEqual(expectedReturn, result);
        }
    }
}