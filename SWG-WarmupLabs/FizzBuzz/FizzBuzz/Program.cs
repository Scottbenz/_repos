﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            string Fizz = "Fizz";
            string Buzz = "Buzz";

            for (int i = 1; i < 101; i++)
            {
                if (i %3 == 0 && i %5 != 0)
                {
                    Console.Write(i);
                    Console.WriteLine(Fizz);
                    i++;
                }
                else if (i %5 == 0 && i %3 != 0 )
                {
                    Console.Write(i);
                    Console.WriteLine(Buzz);
                    i++;
                }
                else if (i %5 == 0 && i %3 == 0)
                {
                    Console.Write(i);
                    Console.WriteLine(Fizz + Buzz);
                    i++;
                }
                else
                {
                    i++;
                }
            }
            Console.ReadLine();
        }
    }
}
