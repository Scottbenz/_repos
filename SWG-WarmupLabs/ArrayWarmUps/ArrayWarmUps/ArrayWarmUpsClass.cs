﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayWarmUps
{
    public class ArrayWarmUpsClass
    {
        //1. FirstLast6b - Given an array of ints, return true if 6 appears as either
        //the first or last element in the array. The array will be length 1 or more. 
        public static bool FirstLast6(int[] numbers)
        {
            return (numbers[0] == 6 || numbers[numbers.Length - 1] == 6);
        }

        //2. SameFirstLast - Given an array of ints, return true if the array is length 1 or more, 
        //and the first element and the last element are equal. 
        public static bool SameFirstLast(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers.Length >= 1 && numbers[0].Equals(numbers.Length - 1 - i))
                {
                    return true;
                }
            }
            return false;

        }

        //3. MakePi - Return an int array length n containing the first n digits of pi.
        public static int[] MakePi(int n)
        {
            int[] firstThreePi = {3, 1, 4}; //Please inform Andrew this is totally not cheating.

            return firstThreePi;
        }

        //4. CommonEnd - Given 2 arrays of ints, a and b, return true if they have the same first 
        //element or they have the same last element. Both arrays will be length 1 or more. 
        public static bool CommonEnd(int[] a, int[] b)
        {
                if (a[0] == b[0])
                {
                    return true;
                }
                if (a[a.Length - 1] == b[b.Length - 1])
                {
                    return true;
                }
                else return false;
        }

        //5. Sum - Given an array of ints, return the sum of all the elements. 
        public static int Sum(int[] numbers)
        {
            int result = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                result += numbers[i];
            }
            return result;
        }

        //6. RotateLeft - Given an array of ints, return an array with the elements 
        //"rotated left" so {1, 2, 3} yields {2, 3, 1}. 
        public static int[] RotateLeft(int[] numbers)
        {
            int[] newint = {0, 0, 0};

            newint[0] += numbers[1];
            newint[1] += numbers[2]; 
            newint[2] += numbers[0];

            return newint;
        }

        //7. Reverse - Given an array of ints length 3, return a new array with the 
        //elements in reverse order, so for example {1, 2, 3} becomes {3, 2, 1}. 
        public static int[] Reverse(int[] numbers)
        {
            int[] newInts = {0, 0, 0};

            for (int i = 0; i < numbers.Length; i++)
            {
                newInts[0 + i] += numbers[2 - i];
            }
            return newInts;
        }

        //8. HigherWins - Given an array of ints, figure out which is larger between 
        //the first and last elements in the array, and set all the other elements to 
        //be that value. Return the changed array. 
        public static int[] HigherWins(int[] numbers)
        {
            int[] newInts = {0, 0, 0};

            if (numbers[0] < numbers[numbers.Length -1])
            {
                for (int i = 0; i < numbers.Length; i++)
                {
                    newInts[i] += numbers.Length;
                }
            }
            if (numbers[0] > numbers[numbers.Length - 1])
            {
                for (int i = 0; i < numbers.Length; i++)
                {
                    newInts[i] += numbers[0];
                }
            }
            return newInts;
        }

        //9. GetMiddle - Given 2 int arrays, a and b, each length 3, return a new array 
        //length 2 containing their middle elements. 
        public static int[] GetMiddle(int[] a, int[] b)
        {
            int[] newInts = {0, 0};

            newInts[0] += a[1];
            newInts[1] += b[1];

            return newInts;
        }

        //10. HasEven - Given an int array , return true if it contains an even number (HINT: Use Mod (%)). 
        public static bool HasEven(int[] numbers)
        {
            for (int i = 0; i < numbers.Length -1; i++)
            {
                if (numbers[i]%2 == 0)
                {
                    return true;
                }
            }
            return false;
        }
        //11. KeepLast - Given an int array, return a new array with double the length where its last element 
        //is the same as the original array, and all the other elements are 0. The original array will be 
        //length 1 or more. Note: by default, a new int array contains all 0's. 
        public static int[] KeepLast(int[] numbers)
        {
            int[] newInts = null;
            
            //Need to resize array to be double size of numbers array.
            Array.Resize(ref newInts, numbers.Length *2);

            //Append last digit from numbers to last spot in newInts.
            newInts[newInts.Length - 1] += numbers[numbers.Length - 1];
            return newInts;
        }
        //12. Double23 - Given an int array, return true if the array contains 2 twice, or 3 twice. 
        public static bool Double23(int[] numbers)
        {
            int countTwo = 0;
            int countThree = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] == 2)
                {
                    countTwo++;
                }
                if (numbers[i] == 3)
                {
                    countThree++;
                }
                if (countTwo == 2 && i == numbers.Length - 1 || countThree == 2 && i == numbers.Length - 1) //Only true if 2 twos.
                {
                    return true;
                }
            }
            return false;
        }
        //13. Fix23
        public static int[] Fix23(int[] numbers)
        {
            for (int i = 0; i < numbers.Length -1; i++)
            {
                if (numbers[i].Equals(2) && numbers[i +1].Equals(3))
                {
                    numbers[i+1] = 0; //If 2 & 3, array
                    return numbers;
                }
            }
            return numbers;
        }
        //14. Unlucky1
        public static bool Unlucky1(int[] numbers)
        {
            if (numbers[0] == 1 && numbers[1] == 3 || numbers[1] == 1 && numbers[2] == 3)
            {
                return true;
            }
            return false;
        }
        //15. Make2
        public static int[] Make2(int[] a, int[] b)
        {
            int[] newArray = {0, 0};

            for (int i = 0; i < 2; i++)
            {
                if (a.Length >= 2)
                {
                    newArray[i] += a[i];
                    newArray[i+1] += a[i+1];
                    return newArray;
                }
                if (a.Length == 1)
                {
                    newArray[0] += a[0];
                    newArray[1] += b[0];
                    return newArray;
                }
                else
                {
                    newArray[0] = b[0];
                    newArray[1] = b[1];
                }
            }
            return newArray;
        }
    }
}
