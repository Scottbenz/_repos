﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using LoopLabs;

namespace LoopTester
{
    public class LoopTest
    {
        [TestFixture]
        public class LoopLabs
        {
            private LoopClass _loopTester;

            //1 StringTimes
            [TestCase("Hi", 2, "HiHi")]
            [TestCase("Hi", 3, "HiHiHi")]
            [TestCase("Hi", 1, "Hi")]
            public void Add_String_Together_n_times(string str, int n, string expectedResult)
            {
                string result = LoopClass.StringTimes(str, n);

                Assert.AreEqual(expectedResult, result);
            }
            //2 FrontTimes
            [TestCase("Chocolate", 2, "ChoCho")]
            [TestCase("Chocolate", 3, "ChoChoCho")]
            [TestCase("Abc", 3, "AbcAbcAbc")]
            public void Add_Front_3Chars_n_Times(string str, int n, string expectedResult)
            {
                string result = LoopClass.FrontTimes(str, n);

                Assert.AreEqual(expectedResult, result);
            }
            //3 CountXX
            [TestCase("abcxx", 1)]
            [TestCase("xxx", 2)]
            [TestCase("xxxx", 3)]
            public void Count_Total_Pairs_ofXX(string str, int expectedResult)
            {
                int result = LoopClass.CountXX(str);

                Assert.AreEqual(expectedResult, result);
            }
            //4 DoubleX
            [TestCase("axxbb", true)]
            [TestCase("axaxxax", false)]
            [TestCase("xxxxx", true)]
            public void DoubleX(string str, bool expectedResult)
            {
                bool result = LoopClass.DoubleX(str);

                Assert.AreEqual(expectedResult, result);
            }
            //5 EveryOther
            [TestCase("Hello", "Hlo")]
            [TestCase("Hi", "H")]
            [TestCase("Heeololeo", "Hello")]
            public void EveryOther_Char_Shown(string str, string expctedResult)
            {
                string result = LoopClass.EveryOther(str);

                Assert.AreEqual(expctedResult, result);
            }
            //6 StringSplosion
            [TestCase("Code", "CCoCodCode")]
            [TestCase("abc", "aababc")]
            [TestCase("ab", "aab")]
            public void StringSplosion(string str, string expectedResult)
            {
                string result = LoopClass.StringSplosion(str);

                Assert.AreEqual(expectedResult, result);
            }
            //7 CountLast2
            [TestCase("hixxhi", 1)]
            [TestCase("xaxxaxaxx", 1)]
            [TestCase("axxxaaxx", 2)]
            public void CountLast2(string str, int expectedResult)
            {
                int result = LoopClass.CountLast2(str);

                Assert.AreEqual(expectedResult, result);
            }
            //8 Count9
            [TestCase(new[] {1, 2, 9}, 1)]
            [TestCase(new[] {1, 9, 9}, 2)]
            [TestCase(new[] {1, 9, 9, 3, 9}, 3)]
            public void Count9(int[] numbers, int expectedResult)
            {
                int result = LoopClass.Count9(numbers);

                Assert.AreEqual(expectedResult, result);
            }
            //9 ArrayFront9
            [TestCase(new[] {1, 2, 9, 3, 4}, true)]
            [TestCase(new[] {1, 2, 3, 4, 9}, false)]
            [TestCase(new[] {1, 2, 3, 4, 5}, false)]
            public void ArrayFront9(int[] numbers, bool expectedResult)
            {
                bool result = LoopClass.ArrayFront9(numbers);

                Assert.AreEqual(expectedResult, result);
            }
            //10 Array123
            [TestCase(new[] {1, 1, 2, 3, 1}, true)]
            [TestCase(new[] {1, 1, 2, 4, 1}, false)]
            [TestCase(new[] {1, 1, 2, 1, 2, 3}, true)]
            public void Array123(int[] numbers, bool expectedResult)
            {
                bool result = LoopClass.Array123(numbers);

                Assert.AreEqual(expectedResult, result);
            }
            //11 SubStringMatch
            [TestCase("xxcaazz", "xxbaaz", 3)]
            [TestCase("abc", "abc", 2)]
            [TestCase("abc", "axc", 0)]
            public void SubStringMatch(string a, string b, int expectedResult)
            {
                int result = LoopClass.SubStringMatch(a, b);

                Assert.AreEqual(expectedResult, result);
            }
            //12. StringX
            [TestCase("xxHxix", "xHix")]
            [TestCase("abxxxcd", "abcd")]
            [TestCase("xabxxxcdx", "xabcdx")]
            public void StringX(string str, string expectedResult)
            {
                string result = LoopClass.StringX(str);

                Assert.AreEqual(expectedResult, result);
            }
            //13. AltPairs
            [TestCase("kitten", "kien")]
            [TestCase("Chocolate", "Chole")]
            [TestCase("CodingHorror", "Congrr")]
            public void AltPairs(string str, string expectedResult)
            {
                string result = LoopClass.AltPairs(str);

                Assert.AreEqual(expectedResult, result);
            }
            //14. DoNotYak
            [TestCase("yakpak", "pak")]
            [TestCase("pakyak", "pak")]
            [TestCase("yak123ya", "123ya")]
            public void DoNotYak_No_Yaks_Allowed(string str, string expectedResult)
            {
                string result = LoopClass.DoNotYak(str);

                Assert.AreEqual(expectedResult, result);
            }
            //15. Array667
            [TestCase(new[] {6, 6, 2}, 1)]
            [TestCase(new[] {6, 6, 2, 6}, 1)]
            [TestCase(new[] {6, 7, 2, 6}, 1)]
            public void Array667_Count_if6or7_inARow(int[] numbers, int expectedResult)
            {
                int result = LoopClass.Array667(numbers);

                Assert.AreEqual(expectedResult, result);
            }
            //16. NoTriples
            [TestCase(new[] {1, 1, 2, 2, 1}, true)]
            [TestCase(new[] {1, 1, 2, 2, 2, 1}, false)]
            [TestCase(new[] {1, 1, 1, 2, 2, 2, 1}, false)]
            public void NoTriples_Return_True_ifNoTriples(int[] numbers, bool expectedResult)
            {
                bool result = LoopClass.NoTriples(numbers);

                Assert.AreEqual(expectedResult, result);
            }
            //17. Pattern51
            [TestCase(new[] {1, 2, 7, 1}, true)]
            [TestCase(new[] {1, 2, 8, 1}, false)]
            [TestCase(new[] {2, 7, 1}, true)]
            public void Pattern51(int[] numbers, bool expectedResult)
            {
                bool result = LoopClass.Pattern51(numbers);

                Assert.AreEqual(expectedResult, result);
            }
        }
    }
}
