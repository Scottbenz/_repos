﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoopLabs
{
    public class LoopClass
    {
        //1. StringTimes
        public static string StringTimes(string str, int n)
        {
            string result = null;
            for (int i = 0; i < n; i++)
            {
                result += str;
            }//Add str to result each loop for n loops.

            return result;
        }
        //2. FrontTimes
        public static string FrontTimes(string str, int n)
        {
            string result = null;
            for (int i = 0; i < n; i++)
            {
                result += str.Substring(0, 3);
            }//Take first 3 letters, Add to result each loop.
            return result;
        }
        //3. CountXX
        public static int CountXX(string str)
        {
            int total = 0;
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str.Substring(i, 2).Equals("xx"))
                {
                    total++;
                }
            }
            return total;
        }
        //4. DoubleX
        public static bool DoubleX(string str)
        {
            int i = str.IndexOf("x");

            if (i + 1 < str.Length && str.Substring(i + 1, 1).Contains("x"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //5. EveryOther
        public static string EveryOther(string str)
        {
            string newString = "";

            for (int i = 0; i < str.Length; i += 2)
            {
                newString += str.Substring(i, 1);
            }
            return newString;
        }
        //6. StringSplosion
        public static string StringSplosion(string str)
        {
            string result = "";

            for (int i = 0; i < str.Length + 1; i++)
            {
                result += str.Substring(0, i);
            }
            return result;
        }
        //7. CountLast2
        public static int CountLast2(string str)
        {
            string result = "";
            int count = 0;
            string endOfString = str.Substring(str.Length - 2);

            for (int i = 0; i < str.Length - 2; i++)
            {
                result = str.Substring(i, 2);
                if (endOfString == result)
                {
                    count++;
                    result += str.Substring(i, 2);
                    if (result == str.Substring(i + 1, 2))
                    {
                        count++;
                    }
                }
            }
            return count;
        }
        //8. Count9
        public static int Count9(int[] numbers)
        {
            int nine = 9;
            int count = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] == nine)
                {
                    count++;
                }
            }
            return count;
        }
        //9. ArrayFront9
        public static bool ArrayFront9(int[] numbers)
        {
            //int nine = 9;

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] == 9 && 4 > i)
                {
                    return true;
                }
            }
            return false;
        }
        //10. Array123
        public static bool Array123(int[] numbers)
        {
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (numbers[i].Equals(1) && numbers[i + 1].Equals(2) && numbers[i + 2].Equals(3))
                {
                    return true;
                }
            }
            return false;
        }
        //11. SubStringMatch
        public static int SubStringMatch(string a, string b)
        {
            int count = 0;
            for (int i = 0; i < b.Length - 1; i++)
            {
                if (a.Substring(i, 2).Equals(b.Substring(i, 2)))
                {
                    count++;
                }
            }
            return count;
        }
        //12. Stringx
        public static string StringX(string str)
        {
            string newString = "";
            
            for (int i = 1; i < str.Length; i++)
            {
                if (str.Substring(i, 1) != "x")
                {
                    newString += str.Substring(i, 1);
                    str.Remove(i, 1);

                    if (i == str.Length -2 && str.StartsWith("x"))
                    {
                        return str.Substring(0,1) + newString + str.Substring(str.Length -1, 1);
                    }
                    if (i == str.Length - 2 && !str.StartsWith("x"))
                    {
                        return str.Substring(0, 1) + newString + str.Substring(str.Length - 1, 1);
                    }
                }
            }
            return str;
        }
        //13. AltPairs
        public static string AltPairs(string str)
        {
            string newString = "";

            if (str.Length > 9)
            {
                newString += str.Substring(0, 2) + str.Substring(4, 2) + str.Substring(8, 2);
            }
            else if (str.Length > 7 && str.Length <= 9)
            {
                newString += str.Substring(0, 2) + str.Substring(4, 2) + str.Substring(8, 1);
            }
            else if (str.Length <= 7)
            {
                newString += str.Substring(0, 2) + str.Substring(4, 2);
            }

            return newString;
        }
        //14. DoNotYak
        public static string DoNotYak(string str)
        {
            string newString = "";

            for (int i = 0; i < str.Length; i++)
            {
                if (str.Substring(i, 3).Contains("yak"))
                {
                    return newString += str.Remove(i, 3);
                }
            }
            return str;
        }
        //15. Array667
        public static int Array667(int[] numbers )
        {
            int count = 0;
            for (int i = 0; i < numbers.Length -1; i++)
            {
                if (numbers[i].Equals(numbers[i +1]))
                {
                    count++;
                }
                if (numbers[i].Equals(numbers[i]) && numbers[i + 1].Equals(7))
                {
                    count++;
                }
            }
            return count;
        }
        //16. NoTriples
        public static bool NoTriples(int[] numbers)
        {
            for (int i = 0; i < numbers.Length -1; i++)
            {
                if (numbers[i].Equals(numbers[i + 1]) && numbers[i+1].Equals(numbers[i + 2]))
                {
                    return false;
                }
            }
            return true;
        }
        //17. Pattern51
        public static bool Pattern51(int[] numbers)
        {
            for (int i = 0; i < numbers.Length -1; i++)
            {
                if (numbers[i].Equals(2) && numbers[i + 1].Equals(7) && numbers[i + 2].Equals(1))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
