﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using WarmupLabs;

namespace WarmupTesters
{
    [TestFixture]
    //name should be same as the class of methods that are being tested.
    public class StringTests
    {
        private StringLabs _stringTester;

        [SetUp]
        public void BeforeEachTest()
        {
            //Arrange
            _stringTester = new StringLabs();
        }
        //1
        [TestCase("Bob", "Hello Bob!")]
        [TestCase("Alice", "Hello Alice!")]
        [TestCase("X", "Hello X!")]
        public void SayHi_ShouldSay_HelloName(string name, string expectedResult)
        {
            //Act
            string result = StringLabs.SayHi(name);

            //Assert
            Assert.AreEqual(expectedResult, result);
        }
        //2
        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        [TestCase("What", "Up", "WhatUpUpWhat")]
        public void ab_Arrange_As_abba(string a, string b, string expectedResult)
        {
            //Act
            string result = StringLabs.Abba(a, b);
            

            //Assert
            Assert.AreEqual(expectedResult, result);
        }
        //3
        [TestCase("i", "Bye", "<i>Bye</i>")]
        [TestCase("Yo", "Alice", "<Yo>Alice</Yo>")]
        [TestCase("What", "Up", "<What>Up</What>")]
        public void MakeTags_Around_Chars(string tag, string content, string expectedResult)
        {
            //Act
            string result = StringLabs.MakeTags(tag, content);

            //Assert
            Assert.AreEqual(expectedResult, result);
        }
        //4
        [TestCase("<<>>", "Yay", "<<Yay>>")] 
        [TestCase("<<>>", "WooHoo", "<<WooHoo>>")]
        [TestCase("[[]]", "word", "[[word]]")]
        public void InsertWord_Into_String(string container, string word, string expectedResult)
        {
            //Act
            string result = StringLabs.InsertWord(container, word);

            //Assert
            Assert.AreEqual(expectedResult, result);
        }
        //5
        [TestCase("Hello", "lololo")]
        [TestCase("ab", "ababab")]
        [TestCase("Hi", "HiHiHi")]
        public void MultipleEndings(string str, string expectedResult)
        {
            //Act
            string result = StringLabs.MultipleEndings(str);

            //Assert
            Assert.AreEqual(expectedResult, result);
        }
        //6
        [TestCase("WooHoo", "Woo")]
        [TestCase("HelloThere", "Hello")]
        [TestCase("abcdef", "abc")]
        public void FirstHalf_Of_String(string str, string expectedResult)
        {
            //Act
            string result = StringLabs.FirstHalf(str);

            //Assert
            Assert.AreEqual(expectedResult, result);
        }
        //7
        [TestCase("Hello", "ell")] 
        [TestCase("java", "av")]
        [TestCase("coding", "odin")]
        public void Trim_1st_Last_Char_FromString(string str, string expectedResult)
        {
            //Act
            string result = StringLabs.TrimOne(str);

            //Assert
            Assert.AreEqual(expectedResult, result);
        }
        //8
        [TestCase("Hello", "hi", "hiHellohi")] 
        [TestCase("hi", "Hello", "hiHellohi")]
        [TestCase("aaa", "b", "baaab")]
        public void Short_Long_Short_Arrangement(string a, string b, string expectedResult)
        {
            //Act
            string result = StringLabs.LongInMiddle(a, b);

            //Assert
            Assert.AreEqual(expectedResult, result);
        }
        //9
        [TestCase("Hello", "lloHe")] 
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void Rotate_First_Two_Chars_ToEnd(string str, string expectedResult)
        {
            string result = StringLabs.Rotateleft2(str);

            Assert.AreEqual(expectedResult, result);
        }
        //10
        [TestCase("Hello", "loHel")] 
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void Rotate_Last_Two_Chars_ToStart(string str, string expectedResult)
        {
            string result = StringLabs.RotateRight2(str);

            Assert.AreEqual(expectedResult, result);
        }
        //11
        [TestCase("Hello", true, "H")] 
        [TestCase("Hello", false, "o")]
        [TestCase("oh", true, "o")]
        public void TakeOne(string str, bool fromFront, string expectedResult)
        {
            string result = StringLabs.TakeOne(str, fromFront);

            Assert.AreEqual(expectedResult, result);
        }
        //12
        [TestCase("string", "ri")] 
        [TestCase("code", "od")]
        [TestCase("Practice", "ct")]
        public void MiddleTwo_Chars_Only(string str, string expectedResult)
        {
            string result = StringLabs.MiddleTwo(str);

            Assert.AreEqual(expectedResult, result);
        }
        //13
        [TestCase("oddly", true)]
        [TestCase("y", false)]
        [TestCase("oddy", false)]
        public void EndsWithLy_Returns_Bool_True(string str, bool expectedResult)
        {
            bool result = StringLabs.EndsWithLy(str);

            Assert.AreEqual(expectedResult, result);
        }
        //14
        [TestCase("Hello", 2, "Helo")]
        [TestCase("Chocolate", 3, "Choate")]
        [TestCase("Chocolate", 1, "Ce")]
        public void FrontAndBack_Return_Frontn_Backn_Chars(string str, int n, string expectedResult)
        {
            string result = StringLabs.FrontAndBack(str, n);

            Assert.AreEqual(expectedResult, result);
        }
        //15
        [TestCase("java", 0, "ja")]
        [TestCase("java", 2, "va")]
        [TestCase("java", 3, "ja")]
        public void TakeTwoFromPosition(string str, int n, string expectedResult)
        {
            string result = StringLabs.TakeTwoFromPosition(str, n);

            Assert.AreEqual(expectedResult, result);
        }
        //16
        [TestCase("badxx", true)]
        [TestCase("xbadxx", true)]
        [TestCase("xxbadxx", false)]
        public void HasBad(string str, bool expectedResult)
        {
            bool result = StringLabs.HasBad(str);

            Assert.AreEqual(expectedResult, result);
        }
        //17
        [TestCase("hello", "he")]
        [TestCase("hi", "hi")]
        [TestCase("h", "h@")]
        public void AtFirst(string str, string expectedResult)
        {
            string result = StringLabs.AtFirst(str);

            Assert.AreEqual(expectedResult, result);
        }
        //18
        [TestCase("last", "chars", "ls")]
        [TestCase("yo", "mama", "ya")]
        [TestCase("hi", "", "h@")]
        public void LastChars(string strA, string strB, string expectedResult)
        {
            string result = StringLabs.LastChars(strA, strB);

            Assert.AreEqual(expectedResult, result);
        }
        //19
        [TestCase("abc", "cat", "abcat")]
        [TestCase("dog", "cat", "dogcat")]
        [TestCase("abc", "", "abc")]
        public void Concat(string a, string b, string expectedResult)
        {
            string result = StringLabs.ConCat(a, b);

            Assert.AreEqual(expectedResult, result);
        }
        //20
        [TestCase("coding", "codign")]
        [TestCase("cat", "cta")]
        [TestCase("ab", "ba")]
        public void SwapLast_Two_Chars_ofString(string str, string expectedResult)
        {
            string result = StringLabs.SwapLast(str);

            Assert.AreEqual(expectedResult, result);
        }
        //21
        [TestCase("edited", true)]
        [TestCase("edit", false)]
        [TestCase("ed", true)]
        public void FirstTwo_Chars_Same_asLastTwo(string str, bool expectedResult)
        {
            bool result = StringLabs.FrontAgain(str);

            Assert.AreEqual(expectedResult, result);
        }
        //22
        [TestCase("Hello", "Hi", "loHi")]
        [TestCase("Hello", "java", "ellojava")]
        [TestCase("java", "Hello", "javaello")]
        public void If_String_GreaterThan_Smaller_Make_itEqualTo(string a, string b, string expectedResult)
        {
            string result = StringLabs.MinCat(a, b);

            Assert.AreEqual(expectedResult, result);
        }
        //23
        [TestCase("Hello", "llo")]
        [TestCase("away", "aay")]
        [TestCase("abed", "abed")]
        public void TweakFront(string str, string expectedResult)
        {
            string result = StringLabs.TweakFront(str);

            Assert.AreEqual(expectedResult, result);
        }
        //24
        [TestCase("xHix", "Hi")]
        [TestCase("xHi", "Hi")]
        [TestCase("Hxix", "Hxi")]
        public void StripX(string str, string expectedResult)
        {
            string result = StringLabs.StripX(str);

            Assert.AreEqual(expectedResult, result);
        }
    }
}
