﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringLabs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(SayHi("Bob"));//1
            Console.WriteLine(SayHi("Alice"));//1
            Console.WriteLine(SayHi("X"));//1
            Console.WriteLine(Abba("Hi", "Bye"));//2
            Console.WriteLine(Abba("Yo", "Alice"));//2
            Console.WriteLine(Abba("What", "Up"));//2
            Console.WriteLine(MakeTags("i", "Yay"));//3
            Console.WriteLine(MakeTags("i", "Hello"));//3
            Console.WriteLine(MakeTags("cite", "Yay"));//3
            Console.WriteLine(InsertWord("<<>>", "Yay"));//4
            Console.WriteLine(InsertWord("<<>>", "WooHoo"));//4
            Console.WriteLine(InsertWord("[[]]", "word"));//4
            Console.WriteLine(MultipleEndings("Hello"));//5
            Console.WriteLine(MultipleEndings("ab"));//5
            Console.WriteLine(MultipleEndings("Hi"));//5
            Console.WriteLine(FirstHalf("WooHoo"));//6
            Console.WriteLine(FirstHalf("HelloThere"));//6
            Console.WriteLine(FirstHalf("abcdef"));//6
            Console.WriteLine(TrimOne("Hello"));//7
            Console.WriteLine(TrimOne("java"));//7
            Console.WriteLine(TrimOne("coding"));//7
            Console.WriteLine(LongInMiddle("Hello", "hi"));//8
            Console.WriteLine(LongInMiddle("hi", "Hello"));//8
            Console.WriteLine(LongInMiddle("aaa", "b"));//8
            Console.WriteLine(Rotateleft2("Hello"));//9
            Console.WriteLine(Rotateleft2("java"));//9
            Console.WriteLine(Rotateleft2("Hi"));//9
            Console.WriteLine(RotateRight2("Hello"));//10
            Console.WriteLine(RotateRight2("java"));//10
            Console.WriteLine(RotateRight2("Hi"));//10
            Console.WriteLine(TakeOne("Hello", true));//11
            Console.WriteLine(TakeOne("Hello", false));//11
            Console.WriteLine(TakeOne("oh", true));//11
            Console.WriteLine(MiddleTwo("string"));//12
            Console.WriteLine(MiddleTwo("code"));//12
            Console.WriteLine(MiddleTwo("Practice"));//12
            Console.WriteLine(EndsWithLy("oddly"));//13
            Console.WriteLine(EndsWithLy("y"));//13
            Console.WriteLine(EndsWithLy("oddy"));//13
            Console.WriteLine(FrontAndBack("Hello", 2));//14
            Console.WriteLine(FrontAndBack("Chocolate", 3));//14
            Console.WriteLine(FrontAndBack("Chocolate", 1));//14
            Console.WriteLine(TakeTwoFromPosition("java", 0));//15
            Console.WriteLine(TakeTwoFromPosition("java", 2));//15
            Console.WriteLine(TakeTwoFromPosition("java", 3));//15
            Console.WriteLine(HasBad("badxx"));//16
            Console.WriteLine(HasBad("xbadxx"));//16
            Console.WriteLine(HasBad("xxbadxx"));//16

            Console.ReadLine();
        }

        //1. SayHi
        public static string SayHi(string name)
        {
            string result = "Hello " + name + "!";
            return result;
        }
        //2. Abba
        public static string Abba(string a, string b) //Randall.
        {
            return (a + b + b + a);
        }
        //3. MakeTags
        public static string MakeTags(string tag, string content)
        {
            string result = "<" + tag + ">" + content + "</" + tag + ">";
            return result;
        }
        //4. InsertWord
        public static string InsertWord(string container, string word)
        {
            string result = container.Substring(0, 2) + word + container.Substring((2));
            return result;
        }
        //5. MultipleEndings
        public static string MultipleEndings(string str)
        {
            string result = str.Substring(str.Length -2);
            return result + result + result;
        }
        //6. FirstHalf
        public static string FirstHalf(string str)
        {
            string result = str.Substring(0, str.Length /2);
            return result;
        }
        //7. TrimOne
        public static string TrimOne(string str)
        {
            string result = str.Substring(1, str.Length -2);
            return result;
        }
        //8. LongInMiddle
        public static string LongInMiddle(string a, string b)
        {
            if (a.Length > b.Length)
            {
                string result = b + a + b;
                return result;
            }
            else
            {
                string result = a + b + a;
                return result;
            }
            
        }
        //9. RotateLeft2
        public static string Rotateleft2(string str)
        {
            string result = str.Substring(2) + str.Substring(0, 2);
            return result;
        }
        //10. RotateRight2
        public static string RotateRight2(string str)
        {
            string result = str.Substring(str.Length -2) + str.Substring(0, str.Length-2);
            return result;
        }
        //11. TakeOne
        public static string TakeOne(string str, bool fromFront)
        {
            if (fromFront == true)
            {
                string result = str.Substring(0, 1);
                return result;
            }
            else
            {
                string result = str.Substring(str.Length - 1);
                return result;
            }
        }
        //12. MiddleTwo - Need middle 2 chars of string.
        public static string MiddleTwo(string str)
        {
            int firstChar = str.Length/2 -1;
            string result = str.Substring(firstChar, 2);
            return result;
        }
        //13. EndsWithLy
        public static bool EndsWithLy(string str)
        {
            if (str.Contains("ly"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //14. FrontAndBack
        public static string FrontAndBack(string str, int n)
        {
            string result = str.Substring(0, n) + str.Substring(str.Length - n);
            return result;
        }

        //15. TakeTwoFromPosition
        public static string TakeTwoFromPosition(string str, int n)
        {
            if (str.Length /2 < n || str.Length /2 > n)
            {
                string result = str.Substring(0, 2);
                return result;
            }
            else
            {
                string result = str.Substring(2);
                return result;
            }
        }

        //16. HasBad
        public static bool HasBad(String str)
        {
            if (str.Equals("bad"))
            {
                return true;
            }
            else if (str.Substring(0, 3).Equals("bad") || str.Substring(1, 3).Equals("bad"))
            {
                return true;
            }
            else return false;
        }

        //17. AtFirst
    }
}
