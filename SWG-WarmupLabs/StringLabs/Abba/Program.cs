﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abba
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = "Hi";
            string b = "Bye";
            Abba(a, b);
            Console.WriteLine();
            Console.ReadLine();
        }

        public static string Abba(string a, string b)
        {
            Console.WriteLine("{0} {1} {1} {0}", a, b);
            return a + b;
        }
    }
}
