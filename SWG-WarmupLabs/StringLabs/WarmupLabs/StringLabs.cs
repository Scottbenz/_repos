﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarmupLabs
{
    public class StringLabs
    {
        static void Main(string[] args)
        {
        }

        //1. SayHi
        public static string SayHi(string name)
        {
            string result = "Hello " + name + "!";
            return result;
        }
        //2. Abba
        public static string Abba(string a, string b)
        {
            return (a + b + b + a);
        }
        //3. MakeTags
        public static string MakeTags(string tag, string content)
        {
            string result = "<" + tag + ">" + content + "</" + tag + ">";
            return result;
        }
        //4. InsertWord
        public static string InsertWord(string container, string word)
        {
            string result = container.Substring(0, 2) + word + container.Substring((2));
            return result;
        }
        //5. MultipleEndings
        public static string MultipleEndings(string str)
        {
            string result = str.Substring(str.Length - 2);
            return result + result + result;
        }
        //6. FirstHalf
        public static string FirstHalf(string str)
        {
            string result = str.Substring(0, str.Length / 2);
            return result;
        }
        //7. TrimOne
        public static string TrimOne(string str)
        {
            string result = str.Substring(1, str.Length - 2);
            return result;
        }
        //8. LongInMiddle
        public static string LongInMiddle(string a, string b)
        {
            if (a.Length > b.Length)
            {
                string result = b + a + b;
                return result;
            }
            else
            {
                string result = a + b + a;
                return result;
            }

        }
        //9. RotateLeft2
        public static string Rotateleft2(string str)
        {
            string result = str.Substring(2) + str.Substring(0, 2);
            return result;
        }
        //10. RotateRight2
        public static string RotateRight2(string str)
        {
            string result = str.Substring(str.Length - 2) + str.Substring(0, str.Length - 2);
            return result;
        }
        //11. TakeOne
        public static string TakeOne(string str, bool fromFront)
        {
            if (fromFront == true)
            {
                string result = str.Substring(0, 1);
                return result;
            }
            else
            {
                string result = str.Substring(str.Length - 1);
                return result;
            }
        }
        //12. MiddleTwo - Need middle 2 chars of string.
        public static string MiddleTwo(string str)
        {
            int firstChar = str.Length / 2 - 1;
            string result = str.Substring(firstChar, 2);
            return result;
        }
        //13. EndsWithLy
        public static bool EndsWithLy(string str)
        {
            if (str.Contains("ly"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //14. FrontAndBack
        public static string FrontAndBack(string str, int n)
        {
            string result = str.Substring(0, n) + str.Substring(str.Length - n);
            return result;
        }

        //15. TakeTwoFromPosition
        public static string TakeTwoFromPosition(string str, int n)
        {
            if (str.Length / 2 < n || str.Length / 2 > n)
            {
                string result = str.Substring(0, 2);
                return result;
            }
            else
            {
                string result = str.Substring(2);
                return result;
            }
        }

        //16. HasBad
        public static bool HasBad(string str)
        {
            if (str.Equals("bad"))
            {
                return true;
            }
            else if (str.Substring(0, 3).Equals("bad") || str.Substring(1, 3).Equals("bad"))
            {
                return true;
            }
            else return false;
        }
        //17. AtFirst
        public static string AtFirst(string str)
        {
            int strLen = str.Length;

            if (strLen >= 2)
            {
                return str.Substring(0, 2);
            }
            else if (strLen < 2)
            {
                return str = string.Concat(str, "@");
            }
            return null;
        }
        //18. LastChars
        public static string LastChars(string strA, string strB)
        {
            int strLenA = strA.Length;
            int strLenB = strB.Length;

            if (strLenA > 1 && strLenB > 1)
            {
                return strA.Substring(0, 1) + strB.Substring((strLenB - 1));
            }
            else if (strLenA == 0)
            {
                return strB.Substring(0, 1) + "@";
            }
            else if (strLenB == 0)
            {
                return strA.Substring(0, 1) + "@";
            }
            return null;
        }
        //19. Concat
        public static string ConCat(string a, string b)
        {
            int aLen = a.Length;
            int bLen = b.Length;

            if (aLen == 0 || bLen == 0)
            {
                return a + b;
            }

            if ((a.Substring(aLen - 1).Equals(b.Substring(0, 1))))
            {
                return a + b.Substring(1);
            }
            else return a + b;
        }
        //20. SwapLast
        public static string SwapLast(string str)
        {
            string lastChar = str.Substring(str.Length - 1);
            string sndLastChar = str.Substring(str.Length - 2, 1);
            string newString = "";

            newString += str.Substring(0, str.Length - 2);
            return newString += lastChar + sndLastChar;
        }
        //21. FrontAgain
        public static bool FrontAgain(string str)
        {
            string firsttwo = str.Substring(0, 2);

            if (firsttwo == str.Substring(str.Length - 2, 2))
            {
                return true;
            }
            return false;
        }
        //22. MinCat
        public static string MinCat(string a, string b)
        {
            string newstring = "";

            if (a.Length > b.Length)
            {
                newstring = a.Substring(a.Length - b.Length) + b;
            }
            else if (b.Length > a.Length)
            {
                newstring = a + b.Substring(b.Length - a.Length);
            }
            return newstring;
        }
        //23. TweakFront
        public static string TweakFront(string str)
        {
            string newString = "";

            if (str.StartsWith("a"))
            {
                newString += "a";
                if (str.Substring(1, 1) == "b")
                {
                    newString += "b";
                }
            }
            return newString + str.Substring(2);
        }
        //24. StripX
        public static string StripX(string str)
        {
            string newString = str;

            if (str.EndsWith("x") && !str.StartsWith("x"))
            {
                newString = newString.Substring(0, newString.Length - 1);
            }
            if (str.StartsWith("x"))
            {
                newString = str.Substring(1);
                if (str.EndsWith("x"))
                {
                    newString = newString.Substring(0, newString.Length -1);
                }
            }
            return newString;
            
        }
    }
}
